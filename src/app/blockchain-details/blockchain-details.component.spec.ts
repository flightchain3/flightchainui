import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockchainDetailsComponent } from './blockchain-details.component';
import {FormsModule} from '@angular/forms';
import {ChartsModule} from 'ng2-charts';
import {NbCardModule, NbLayoutModule, NbStepperModule, NbThemeModule} from '@nebular/theme';
import {HttpClient} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';

describe('BlockchainDetailsComponent', () => {
  const mockHttpClient =  jasmine.createSpyObj('httpClient', ['get']);
  let component: BlockchainDetailsComponent;
  let fixture: ComponentFixture<BlockchainDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: mockHttpClient }
      ],
      imports: [
        RouterTestingModule,
        NbLayoutModule,
        NbCardModule,
        NbStepperModule,
        NbThemeModule.forRoot({name: 'cosmic'}),
        FormsModule,
        ChartsModule
      ],
      declarations: [ BlockchainDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockchainDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
