import {IFlightChainData} from './IFlightChainData';

export interface IFlightChainDataHistory {
    value: IFlightChainData;
    id_deleted: boolean;
    tx_id: string;
    timestamp: any;
}
