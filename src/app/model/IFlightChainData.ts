import {ACRISFlight} from './ACRISFlight';

/**
 * This is the data model stored on the blockchain.
 * Each ACRIS flight entry/update is stored with the transaction Id and
 * updater Id so it can conveniently be accessed by the client apps.
 */
export interface IFlightChainData {
    flightData: ACRISFlight;
    flightKey: string;
    // Which IATA entity updated the data.
    updaterId: string;
    txId: string;
    docType: string;
}
