import { TestBed, inject } from '@angular/core/testing';

import { AppService } from './app.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment.prod';

describe('AppService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule
    ],
    providers: [
      AppService
    ]
  }));

  it('should be created', inject([AppService], (service: AppService) => {
    expect(service).toBeTruthy();
  }));

  describe('getFlightByKey', () => {
    it('should perform an http get with url based on supplied flight key',
      inject([AppService, HttpClient, HttpTestingController],
        (service: AppService, http: HttpClient, httpMock: HttpTestingController) => {

          service.getFlightByKey('someKey')
            .subscribe(flights => expect(flights.length).toEqual(2));

          const req = httpMock.expectOne(environment.getURL + 'someKey');

          req.flush(
            [{flight: 1}, {flight: 2}]
          );
    }));

    it('should return empty array if not matching flight found',
      inject([AppService, HttpClient, HttpTestingController],
        (service: AppService, http: HttpClient, httpMock: HttpTestingController) => {

          service.getFlightByKey('someKey')
            .subscribe(flights => expect(flights.length).toEqual(0));

          const req = httpMock.expectOne(environment.getURL + 'someKey');

          req.flush(
            []
          );
        }));

    it('should return an error if service call fails',
      inject([AppService, HttpClient, HttpTestingController],
        (service: AppService, http: HttpClient, httpMock: HttpTestingController) => {

          service.getFlightByKey('someKey')
            .subscribe(flights => fail('unexpected service success'),
              err => expect(err.status).toEqual(404));

          const req = httpMock.expectOne(environment.getURL + 'someKey');

          req.flush({}, { status: 404, statusText: 'some bad happened'});
        }));
  });
});
