import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { IFlightChainData } from './model/IFlightChainData';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  styles: [
    `:host nb-layout-column { width:80vw}`
  ]
})

export class AppComponent {
  constructor(private appService: AppService) {
  }

  // TODO move the flight stuff down to a separate component similar to arrival-airport, etc and leave this component light
  flightkey: string; // = "2019-01-07LHRBA0749";
  title = 'flight-chain-ui';
  formControl = new FormControl(new Date());
  dateInput = new Date();
  flights: IFlightChainData[];
  message: string;

  callGetFlightsByKey(): void {
    console.log('callGetFlights: ' + this.flightkey);
    this.appService.getFlightByKey(this.flightkey)
      .subscribe(flights => {
        if (flights.length === 0) {
          this.message = 'No flight found matching supplied flight key: ' + this.flightkey;
        }
        this.flights = flights;
      }, err => {
        if (err.status && err.status === 404) {
          this.message = 'No flight found matching supplied flight key: ' + this.flightkey;
        } else {
          this.message = 'failed to read flight data';
        }
      });
  }

  callGetFlightsByDate(): void {
    console.log('callGetFlights: ' + this.dateInput);
    this.appService.getFlightByDate(this.dateInput)
      .subscribe(flights => this.flights = flights);
  }
}
