import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ChartOptions, ChartType } from 'chart.js';
import { Label, SingleDataSet } from 'ng2-charts';
import { HttpClient } from '@angular/common/http';
import { IFlightChainData } from '../model/IFlightChainData';

@Component({
  selector: 'app-arrival-airport',
  templateUrl: './arrival-airport.component.html',
  styleUrls: ['./arrival-airport.component.css']
})

export class ArrivalAirportComponent {

  formControl = new FormControl(new Date());
  ngModelDate = new Date();
  flights: any;   // IFlightChainData[];
  url = 'http://localhost:3000/flightChain/';   // TODO use environment
  constructor(private http: HttpClient) { }

  public pieChartOptions: ChartOptions = {
    responsive: true,
  };

  public pieChartLabelsArrival: Label[] = ['GVA', 'LHR', 'CDG', 'AMS', 'FRA', 'DUB'];
  public pieChartDataArrival: SingleDataSet = [60, 150, 31, 76, 56, 20];
  public pieChartTypeArrival: ChartType = 'pie';
  public pieChartLegendArrival = true;
  public pieChartPluginsArrival = [];

  async loadFlights() {
    // TODO refactor this to use a service
    this.flights = await this.http.get<IFlightChainData[]>(this.url + 'range/2019-03-13/2019-03-14ZZ').toPromise();
    const arrivalAirport: { [airportCode: string]: number; } = {};
    for (const flight of this.flights) {
      if (flight.flightData.arrivalAirport in arrivalAirport) {
        arrivalAirport[flight.flightData.arrivalAirport]++;
      } else {
        arrivalAirport[flight.flightData.arrivalAirport] = 1;
      }
    }
    const sortedArrivalAirportKeys = Object.keys(arrivalAirport).sort((a, b) => {
      return arrivalAirport[b] - arrivalAirport[a];
    }).slice(0, 10);


    this.pieChartLabelsArrival = sortedArrivalAirportKeys;

    const sortedArrivalAirportValue: number[] = [];

    for (const item of sortedArrivalAirportKeys) {
      sortedArrivalAirportValue.push(arrivalAirport[item]);
    }
    this.pieChartDataArrival = sortedArrivalAirportValue;
  }

}
