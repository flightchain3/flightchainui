import { Component } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-blockchain-trends',
  templateUrl: './blockchain-trends.component.html',
  styleUrls: ['./blockchain-trends.component.css']
})
export class BlockchainTrendsComponent{
  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [
        { ticks: { beginAtZero: true } }
      ]
    }
  };

  public barChartLabels: Label[] = ['10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 80], label: 'Transactions' },
    { data: [58, 48, 62, 61, 46, 53, 70], label: 'Blocks' }
  ];

}
