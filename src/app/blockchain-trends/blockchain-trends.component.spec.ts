import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockchainTrendsComponent } from './blockchain-trends.component';
import {FormsModule} from '@angular/forms';
import {ChartsModule} from 'ng2-charts';
import {NbCardModule, NbLayoutModule, NbStepperModule, NbThemeModule} from '@nebular/theme';
import {HttpClient} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';

describe('BlockchainTrendsComponent', () => {
  const mockHttpClient = jasmine.createSpyObj('httpClient', ['get']);
  let component: BlockchainTrendsComponent;
  let fixture: ComponentFixture<BlockchainTrendsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: mockHttpClient }
      ],
      imports: [
        RouterTestingModule,
        NbLayoutModule,
        NbCardModule,
        NbStepperModule,
        NbThemeModule.forRoot({name: 'cosmic'}),
        FormsModule,
        ChartsModule
      ],
      declarations: [ BlockchainTrendsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockchainTrendsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
