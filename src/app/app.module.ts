import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
  NbLayoutModule, NbSidebarModule, NbSidebarService, NbThemeModule, NbActionsModule,
  NbUserModule, NbDatepickerModule, NbInputModule, NbCardModule, NbTabsetModule, NbStepperModule
} from '@nebular/theme';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ChartsModule} from 'ng2-charts';
import { HttpClientModule } from '@angular/common/http';
import { ArrivalAirportComponent } from './arrival-airport/arrival-airport.component';
import { BlockchainDetailsComponent } from './blockchain-details/blockchain-details.component';
import { BlockchainTrendsComponent } from './blockchain-trends/blockchain-trends.component';
import { DepartureAirportComponent } from './departure-airport/departure-airport.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ArrivalAirportComponent,
    BlockchainDetailsComponent,
    BlockchainTrendsComponent,
    DepartureAirportComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    NbThemeModule.forRoot({name: 'cosmic'}),
    NbLayoutModule,
    NbActionsModule,
    NbSidebarModule,
    NbUserModule,
    NbDatepickerModule.forRoot(),
    NbInputModule,
    NbCardModule,
    NbTabsetModule,
    NbStepperModule,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [NbSidebarService],
  bootstrap: [AppComponent]
})
export class AppModule { }
