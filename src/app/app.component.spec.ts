import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import {AppService} from './app.service';
import {
  NbActionsModule, NbCardModule, NbDatepickerModule, NbLayoutModule, NbStepperModule, NbTabsetModule, NbThemeModule,
  NbUserModule
} from '@nebular/theme';
import {ArrivalAirportComponent} from './arrival-airport/arrival-airport.component';
import {DepartureAirportComponent} from './departure-airport/departure-airport.component';
import {BlockchainTrendsComponent} from './blockchain-trends/blockchain-trends.component';
import {FormsModule} from '@angular/forms';
import {ChartsModule} from 'ng2-charts';
import {HttpClient} from '@angular/common/http';
import {of} from 'rxjs';
import {IFlightChainData} from './model/IFlightChainData';

describe('AppComponent', () => {
  const mockAppService = jasmine.createSpyObj('appService', ['getFlightByKey', 'getFlightByDate']);
  const mockHttpClient = jasmine.createSpyObj('httpClient', ['get']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: AppService, useValue: mockAppService },
        { provide: HttpClient, useValue: mockHttpClient }
      ],
      imports: [
        RouterTestingModule,
        NbLayoutModule,
        NbUserModule,
        NbActionsModule,
        NbCardModule,
        NbTabsetModule,
        NbDatepickerModule,
        NbStepperModule,
        NbThemeModule.forRoot({name: 'cosmic'}),
        ChartsModule,
        FormsModule
      ],
      declarations: [
        AppComponent,
        ArrivalAirportComponent,
        DepartureAirportComponent,
        BlockchainTrendsComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should have as title flight-chain-ui', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('flight-chain-ui');
  });

  describe('getFlightsByKey', () => {
    it('should call service to get flights', () => {
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.debugElement.componentInstance;

      mockAppService.getFlightByKey.and.returnValue(of([]));
      app.flightkey = 'someKey123';
      app.callGetFlightsByKey();

      expect(mockAppService.getFlightByKey).toHaveBeenCalled();
    });

    it('should set flights property when flight returned from service', () => {
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.debugElement.componentInstance;

      mockAppService.getFlightByKey.and.returnValue(of([{ flightData: {}, flightKey: 'fk123' } as IFlightChainData]));
      app.flightkey = 'someKey123';
      app.callGetFlightsByKey();

      expect(app.flights[0].flightKey).toEqual('fk123');
    });

    it('should set message when no flights returned from service', () => {
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.debugElement.componentInstance;

      mockAppService.getFlightByKey.and.returnValue(of([]));
      app.flightkey = 'someKey123';
      app.callGetFlightsByKey();

      expect(app.message).toEqual('No flight found matching supplied flight key: someKey123');
    });
  });
});
