import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import {catchError, mergeMap} from 'rxjs/operators';

import { environment } from '../environments/environment';
import {AuthService} from './auth/auth.service';
import { IFlightChainData } from './model/IFlightChainData';


@Injectable({ providedIn: 'root'})
export class AppService {

  API_URL = environment.apiURL;
  GET_URL = environment.getURL;

  constructor(private http: HttpClient, private authService: AuthService) {
  }

  public getFlightByKey(flightKey: string): Observable<IFlightChainData[]> {
    const query = this.GET_URL + flightKey;
    // TODO temp hardcode
    return this.authService.login('saa', 'saaPassw0rd!')
      .pipe(mergeMap(authResponse => {
        console.log('I got the JWT: ' + authResponse.jwt);
        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + authResponse.jwt
          })
        };

        return this.http.get<IFlightChainData[]>(query, httpOptions)
          .pipe(catchError(this.handleError));
      }));
  }

  public getFlightByDate(originDate: Date): Observable<IFlightChainData[]> {
    const query = {selector: {'flightData.originDate': originDate }};

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.post<IFlightChainData[]>(this.API_URL, JSON.stringify(query), httpOptions)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred: ', error.error.message);
    } else {
      console.error('Backend returned error code: ' + error.status);
    }
    return throwError(error);
  }
}
