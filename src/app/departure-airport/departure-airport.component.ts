import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ChartOptions, ChartType } from 'chart.js';
import { Label, SingleDataSet } from 'ng2-charts';
import { HttpClient } from '@angular/common/http';
import { IFlightChainData } from '../model/IFlightChainData';

@Component({
  selector: 'app-departure-airport',
  templateUrl: './departure-airport.component.html',
  styleUrls: ['./departure-airport.component.css']
})
export class DepartureAirportComponent {

  formControl = new FormControl(new Date());
  ngModelDate = new Date();
  flights: any;     // IFlightChainData[];
  url = 'http://localhost:3000/flightChain/';
  constructor(private http: HttpClient) { }

  public pieChartOptions: ChartOptions = {
    responsive: true,
  };

  public pieChartLabelsDeparture: Label[] = ['GVA', 'LHR', 'CDG', 'AMS', 'FRA', 'DUB'];
  public pieChartDataDeparture: SingleDataSet = [60, 150, 31, 76, 56, 20];
  public pieChartTypeDeparture: ChartType = 'pie';
  public pieChartLegendDeparture = true;
  public pieChartPluginsDeparture = [];


  async loadFlights() {
    this.flights = await this.http.get<IFlightChainData[]>(this.url + 'range/2019-03-13/2019-03-14ZZ').toPromise();
    const DepartureAirport: { [airportCode: string]: number; } = {};
    for (const flight of this.flights) {
      if (flight.flightData.DepartureAirport in DepartureAirport) {
        DepartureAirport[flight.flightData.DepartureAirport]++;
      } else {
        DepartureAirport[flight.flightData.DepartureAirport] = 1;
      }
    }
    const sortedDepartureAirportKeys = Object.keys(DepartureAirport).sort( (a, b) => {
      return DepartureAirport[b] - DepartureAirport[a];
    }).slice(0, 10);


    this.pieChartLabelsDeparture = sortedDepartureAirportKeys;

    const sortedDepartureAirportValue: number[] = [];

    for (const item of sortedDepartureAirportKeys) {
      sortedDepartureAirportValue.push(DepartureAirport[item]);
    }
    this.pieChartDataDeparture = sortedDepartureAirportValue;
  }

}
