export const environment = {
  production: true,
  authURL: '/auth/',
  apiURL: '/flightChain/find',
  getURL: '/flightChain/'
};
